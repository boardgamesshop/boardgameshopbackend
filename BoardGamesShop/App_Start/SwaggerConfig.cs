using System.Web.Http;
using WebActivatorEx;
using BoardGamesShop;
using Swashbuckle.Application;
using System.Linq;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace BoardGamesShop
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "BoardGamesShop");
                        c.PrettyPrint();
                        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                        c.GroupActionsBy(apiDesc => apiDesc.RelativePath.ToString());
                        c.IncludeXmlComments($"{System.AppDomain.CurrentDomain.BaseDirectory}/App_Data/BoardGamesShopDocuments.xml");
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
