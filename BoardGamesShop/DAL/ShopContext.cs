﻿using BoardGamesShop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BoardGamesShop.DAL
{
    public class ShopContext :DbContext
    {
        public ShopContext() : base("ShopContext")
        {
        }
        
        public DbSet<GameImport> GameImports { get; set; }
        public DbSet<GameInfo> GameInfos { get; set; }
        public DbSet<GameInStock> GameInStocks { get; set; }
        public DbSet<GameOrder> GameOrders { get; set; }
        public DbSet<GameOrderInfo> GameOrderInfos { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserComment> UserComments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GameInfo>()
                .HasRequired(x => x.GameInStock)
                .WithRequiredPrincipal(x => x.GameInfo);
        }
    }
}