﻿using System;
using BoardGamesShop.Logic;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoardGamesShop.Models;
using BoardGamesShop.DAL;
using System.Web.Http.Description;

namespace BoardGamesShop.Controllers.api
{
    public class OrdersController : ApiController
    {
        OrderList orderList = new OrderList();

      //  public IHttpActionResult GetOrders() =>
      //      Ok(orderList.FullOrderList());

        public IHttpActionResult GetOrderById(Guid id)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, orderList.OrderById(id));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetOrdersByUserId(Guid id)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, orderList.OrdersByUserId(id));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetOrderByStatus(OrderStatus status)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, orderList.OrderByStatus(status));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetOrderByDate(DateTime date)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, orderList.OrderByDate(date));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetOrdersSortedByDate()
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, orderList.OrdersSortedByDate());
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        [ResponseType(typeof(GameOrder))]
        public IHttpActionResult PostOrder(Guid token, string address, List<(GameInfo Game, int Count)> orderGames)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
            User user = UserSession.UserByToken(token);
            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));

            using (var db = new ShopContext())
            {  
                db.GameOrders.Add(orderList.CreatedOrder(user,address,orderList.gameOrderInfos(orderGames)));
                db.SaveChanges();
            }
            return Ok(JObject.FromObject(new { status = "ok" }));
        }

        public IHttpActionResult PostChangeOrderStatus(Guid token, Guid orderId, OrderStatus orderStatus)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
                //проверка прав
                var Rights = UserSession.UserPosition(token);
                if (Rights < Position.OrderManager)
                    return this.StatusCodeWithJson(HttpStatusCode.Forbidden, JObject.FromObject(new { status = "fail", message = "you have no rights" }));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }

            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));

            using (var db = new ShopContext())
            {
                var order = db.GameOrders.FirstOrDefault(x => x.Id == orderId);
                if (order == null)
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "Order not found" }));
                order.Status = orderStatus;
                db.SaveChanges();
                return Ok(JObject.FromObject(new { status = "ok" }));
            }
        }

        //public IHttpActionResult CreateOrder(Guid token, List<GameOrderInfo> newGames, string address)
        //{
        //    var session = UserSession.UserSessions.FirstOrDefault(x => x.Token == token);
        //    if (!session.IsActive)
        //        return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "Session is inactive" }));
        //    var user = session.User;
        //    if (!ModelState.IsValid)
        //        return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));
        //    using (var db = new ShopContext())
        //    {
        //        var order = orderList.CreateOrder(user, address,newGames);
        //        db.GameOrders.Add(order);
        //        db.SaveChanges();
        //    }
        //    return Ok(JObject.FromObject(new { status = "ok" }));
        //}
    }
}
