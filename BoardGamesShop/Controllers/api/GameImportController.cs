﻿using BoardGamesShop.DAL;
using BoardGamesShop.Logic;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoardGamesShop.Controllers.api
{
    public class GameImportController : ApiController
    {
        GameImportList gameImportList = new GameImportList();

        public IHttpActionResult GetGameImports()
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, gameImportList.FullGameImportList());
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult PostGameImport(Guid token, List<(Guid, int)> games)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
                var Rights = UserSession.UserPosition(token);
                if (Rights!= Position.Reviewer||Rights!=Position.Admin)
                    return this.StatusCodeWithJson(HttpStatusCode.Forbidden, JObject.FromObject(new { status = "fail", message = "you have no rights" }));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
            
            User user = UserSession.UserByToken(token);
            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));
            try
            {
                using (var db = new ShopContext())
                { 
                    return this.StatusCodeWithJson(HttpStatusCode.OK, JObject.FromObject(gameImportList.CreatedImport(games)));
                }
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Forbidden, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }
    }
}
