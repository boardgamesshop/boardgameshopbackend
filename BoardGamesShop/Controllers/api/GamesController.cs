﻿using BoardGamesShop.DAL;
using BoardGamesShop.Logic;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoardGamesShop.Controllers
{
    public class GamesController : ApiController
    {
        GameList gameList = new GameList();

        public IHttpActionResult GetGameById(Guid id)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, gameList.GameById(id));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status="fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetGameByName(string name)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, gameList.GameByName(name));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetGamesWithParameters(bool inStock = false, int minNum = 100,int maxNum = 0,int minAge = 100, GameLevel lvl = GameLevel.Any, float minPrice = 0,float maxPrice = 10000, int minTime = 10000, int maxTime = 0)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, gameList.GameWithParameters(inStock, minNum, maxNum, minAge, lvl, minPrice, maxPrice, minTime, maxTime));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult PostGame(Guid token, GameInfo gameInfo)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
                var Rights = UserSession.UserPosition(token);
                if (Rights < Position.Reviewer)
                    return this.StatusCodeWithJson(HttpStatusCode.Forbidden, JObject.FromObject(new { status = "fail", message = "you have no rights" }));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }

            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));

            using (var db = new ShopContext())
            {

                db.GameInfos.Add(gameInfo);
              //  db.GameInStocks.Add(new GameInStock { GameInfo = gameInfo, QuantityInStock = 0 });

                db.SaveChanges();
            }
            return Ok(JObject.FromObject(new { status = "ok" }));
        }
        public IHttpActionResult PostGameWithInfo(Guid token, string name, string picture, int minNumberOfPlayers, int maxNumberOfPlayers, int minAge, int minTime, int maxTime, string description, string author, GameLevel level, float price)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
                var Rights = UserSession.UserPosition(token);
                if (Rights < Position.Reviewer)
                    return this.StatusCodeWithJson(HttpStatusCode.Forbidden, JObject.FromObject(new { status = "fail", message = "you have no rights" }));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }

            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));

            using (var db = new ShopContext())
            {
                GameInfo newGame = gameList.NewGame(name, picture, minNumberOfPlayers, maxNumberOfPlayers, minAge, minTime, maxTime, description, author, level, price);
                db.GameInfos.Add(newGame);
                db.GameInStocks.Add(new GameInStock { GameInfo=newGame,QuantityInStock=0});
                db.SaveChanges();
            }
            return Ok(JObject.FromObject(new { status = "ok" }));
        }

    }
}
