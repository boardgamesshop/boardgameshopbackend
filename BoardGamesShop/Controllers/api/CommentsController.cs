﻿using System;
using BoardGamesShop.Logic;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoardGamesShop.Models;
using BoardGamesShop.DAL;

namespace BoardGamesShop.Controllers.api
{
    public class CommentsController : ApiController
    {
        CommentList commentList = new CommentList();

        public IHttpActionResult GetComments() =>
            Ok(commentList.FullCommentList());

        public IHttpActionResult GetCommentByUserId(Guid id)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, commentList.CommentByUserId(id));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult PostOrder(Guid token, string text)
        {
            try
            {
                //проверка, есть ли токен (активна ли сессия)
                if (!UserSession.IsActive(token))
                {
                    return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = "token not found" }));
                }
                UserSession.MakeActive(token);
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
            Guid userId = UserSession.UserByToken(token).Id;
            if (!ModelState.IsValid)
                return this.StatusCodeWithJson(HttpStatusCode.Ambiguous, JObject.FromObject(new { status = "fail", message = "object is not correct" }));

            using (var db = new ShopContext())
            {
                db.UserComments.Add(commentList.NewComment(userId, text));
                db.SaveChanges();
            }
            return Ok(JObject.FromObject(new { status = "ok" }));
        }

    }
}
