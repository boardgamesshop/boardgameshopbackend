﻿using BoardGamesShop.Logic;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoardGamesShop.Controllers.api
{
    public class AuthController : ApiController
    {
        public struct Data
        {
            public string Login;
            public string Password;
        }

        public IHttpActionResult PostAuth(string login, string password)
        {
            try
            {
                var token = UserSession.SetSession(login, password);
                return Ok(JObject.FromObject(new { status = "ok", token, position = UserSession.UserPosition(token) }));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        //public IHttpActionResult PostAuth(Data data)
        //{
        //    try
        //    {
        //        var token = UserSession.SetSession(data.Login, data.Password);
        //        return Ok(JObject.FromObject(new { status = "ok", token, position = UserSession.UserPosition(token) }));
        //    }
        //    catch (Exception E)
        //    {
        //        return this.StatusCodeWithJson(HttpStatusCode.Unauthorized, JObject.FromObject(new { status = "fail", message = E.Message }));
        //    }
        //}
    }
}
