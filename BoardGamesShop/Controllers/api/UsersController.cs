﻿using System;
using BoardGamesShop.Logic;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoardGamesShop.Controllers.api
{
    public class UsersController : ApiController
    {
        UserList userList = new UserList();

        public IHttpActionResult GetUsers() =>
            Ok(userList.FullUserList());

        public IHttpActionResult GetUserById(Guid id)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, userList.UserById(id));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        public IHttpActionResult GetUserByOrderId(Guid orderId)
        {
            try
            {
                return this.StatusCodeWithJson(HttpStatusCode.Found, userList.UserByOrderId(orderId));
            }
            catch (Exception E)
            {
                return this.StatusCodeWithJson(HttpStatusCode.NotFound, JObject.FromObject(new { status = "fail", message = E.Message }));
            }
        }

        
    }
}
