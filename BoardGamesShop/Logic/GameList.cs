﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    public class GameList
    {
        ShopContext db = new ShopContext();

      /*  public JArray FullGameList()
        {
            var games = db.GameInfos.ToList().Select(x => x.ToJson());
            return JArray.FromObject(games);
        }*/

        public JObject GameById(Guid id)
        {
            var game = db.GameInfos.FirstOrDefault(x => x.Id == id);
            if (game == null)
                throw new Exception("Game not found.");
            return JObject.FromObject(game.ToJson());
        }

        public JArray GameByName(string name)
        {
            var games = db.GameInfos.Where(x => x.Name.Contains(name))
                        .ToList().Select(x=>x.ToJson());
            if (games == null)
                throw new Exception("Games not found.");
            return JArray.FromObject(games);
        }

        public JArray GameWithParameters(bool inStock = false, int minNum = 100, int maxNum = 0, int minAge = 100, GameLevel lvl = GameLevel.Any, float minPrice = 0, float maxPrice = 10000, int minTime = 10000, int maxTime = 0)
        {
            //соотносим уровень игры с цифрой
            //ищем игру по параметрам
            var games = db.GameInfos
                .Where(x => (
                x.MinNumberOfPlayers <= minNum && x.MaxNumberOfPlayers >= maxNum //количество игроков 
                && x.MinAge <= minAge                                            //возраст
                && (x.Level & lvl) == x.Level                                    //уровень
                && x.GameInStock.QuantityInStock > (inStock ? 0 : -1) 
                && x.Price >= minPrice && x.Price <= maxPrice                    //цена
                && x.MinTime <= minTime && x.MaxTime >= maxTime))                //время
                .ToList().Select(x => x.ToJson());
            if (games == null)
                throw new Exception("Games not found.");
            return JArray.FromObject(games);
        }

        public GameInfo NewGame(string name, string picture, int minNumberOfPlayers, int maxNumberOfPlayers, int minAge, int minTime, int maxTime, string description, string author, GameLevel level, float price)
        {
            GameInfo newGame = new GameInfo
            {
                Name = name,
                Picture = picture,
                MinNumberOfPlayers = minNumberOfPlayers,
                MaxNumberOfPlayers = maxNumberOfPlayers,
                MinAge = minAge,
                MinTime = minTime,
                MaxTime = maxTime,
                Description = description,
                Author = author,
                Level = level,
                Price = price
            };
            return newGame;
        }
    }
}