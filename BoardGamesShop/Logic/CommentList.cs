﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    public class CommentList
    {
        ShopContext db = new ShopContext();

        public JArray FullCommentList()
        {
            var comments = db.UserComments.ToList().Select(x => x.ToJson());
            return JArray.FromObject(comments);
        }

        /*public JObject CommentById(Guid id)
          {
              var comment = db.UserComments.FirstOrDefault(x => x.Id == id);
              if (comment == null)
                  throw new Exception("Comment not found.");
              return JObject.FromObject(comment.ToJson());
          }
          */

        public JArray CommentByUserId(Guid id)
        {
            var comments = db.UserComments.Where(x => x.UserId == id)
                .ToList().Select(x => x.ToJson());
            if (comments == null)
                throw new Exception("Comments not found.");
            return JArray.FromObject(comments);
        }

        public UserComment NewComment(Guid userId, string text)
        {
            UserComment comment = new UserComment
            {
                UserId = userId,
                Comment = text
            };
            return comment;
        }
    }
}