﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    internal static class Encryption
    {
        public static string GetPasshash(string text)
        {
            if (String.IsNullOrEmpty(text))
                return String.Empty;

            using (var sha = new System.Security.Cryptography.SHA256Managed())
            {
                byte[] textData = System.Text.Encoding.UTF8.GetBytes(text);
                byte[] hash = sha.ComputeHash(textData);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }
    }
}