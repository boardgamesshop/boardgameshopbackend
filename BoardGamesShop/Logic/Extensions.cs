﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BoardGamesShop
{
    [Flags]
    public enum GameLevel
    {
        Easy = 1,
        Middle = 2,
        Hard = 4,
        Any = Easy | Middle | Hard
    }

    public enum OrderStatus
    {
        Registered,
        Sent,
        Received
    }

    public enum Position
    {
        Admin = 1000,
        OrderManager = 500,
        Reviewer = 200,
        Bookkeeper = 100,
        Customer = 0
    }

    static class Extentions
    {
        public static IHttpActionResult StatusCodeWithJson(this ApiController apiController, HttpStatusCode code, JToken content) =>
            new CodeWithObjectResult(code, content);
    }

    internal class CodeWithObjectResult : IHttpActionResult
    {
        HttpResponseMessage InnerMessage { get; }      
        public CodeWithObjectResult(HttpStatusCode code, JToken content) =>
             InnerMessage = new HttpResponseMessage(code) { Content = new ObjectContent<JToken>(content, new JsonMediaTypeFormatter()) }; 
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken) =>
            Task.Run(() => InnerMessage);
    }

}