﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    public class UserList
    {
        ShopContext db = new ShopContext();

        public JArray FullUserList()
        {
            var users = db.Users.ToList().Select(x => x.ToJson());
            return JArray.FromObject(users);
        }

        public JObject UserById(Guid id)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == id);
            if (user == null)
                throw new Exception("User not found.");
            return JObject.FromObject(user.ToJson());
        }

        public JObject UserByOrderId(Guid id)
        {
            var user = db.Users.Where(x => (x.GameOrders.FirstOrDefault(y => y.Id == id) != null)).Select(x=>x.ToJson());
            if (user == null)
                throw new Exception("User not found.");
            return JObject.FromObject(user);
        }

        
    }
}