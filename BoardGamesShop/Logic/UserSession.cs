﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BoardGamesShop.Logic
{
    internal static class UserSession
    {
        struct Session
        {
            public Guid Token { get; set; }
            public User User { get; set; }
            public DateTime LastActivity { get; set; }
            public bool IsActive => DateTime.Now - LastActivity < TimeSpan.FromHours(1);
        }
        static List<Session> UserSessions { get; set; } = new List<Session>();

        public static Guid SetSession(string login, string password)
        {
            Task.Run(() => UserSessions.RemoveAll(x => !x.IsActive));
            using (var db = new ShopContext())
            {
                var passhash = Encryption.GetPasshash(password);
                var potentialUser = db.Users.FirstOrDefault(x => x.Login == login && x.Passhash == passhash);
                if (potentialUser == null)
                    throw new UnauthorizedAccessException("Wrong credentials");
                var newSession = new Session()
                {
                    User = potentialUser,
                    Token = Guid.NewGuid(),
                    LastActivity = DateTime.Now
                };
                UserSessions.Add(newSession);
                return newSession.Token;
            }
        }
       
        public static Position UserPosition(Guid token)
        {
            var potentialSession = UserSessions.FirstOrDefault(x => x.Token == token);
            if (!potentialSession.IsActive)
                throw new UnauthorizedAccessException("Wrong token");

            potentialSession.LastActivity = DateTime.Now;
            return potentialSession.User.AccessLevel;            
        }

        public static bool IsActive(Guid token)
        {
            var potentialSession = UserSessions.FirstOrDefault(x => x.Token == token);
            if (!potentialSession.IsActive)
                return false;
            potentialSession.LastActivity = DateTime.Now;
            return true;
        }
        
        public static User UserByToken(Guid token)
        {
            var potentialSession = UserSessions.FirstOrDefault(x => x.Token == token);
            if (!potentialSession.IsActive)
                throw new UnauthorizedAccessException("Wrong token");

            potentialSession.LastActivity = DateTime.Now;
            return potentialSession.User;
        }

        public static void MakeActive(Guid token)
        {
            var session = UserSessions.FirstOrDefault(x => x.Token == token);
            session.LastActivity = DateTime.Now;
        }
    }
}