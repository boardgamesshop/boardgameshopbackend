﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    public class GameImportList
    {
        ShopContext db = new ShopContext();

        public JArray FullGameImportList()
        {
            var games = db.GameImports.ToList().Select(x => x.ToJson());
            return JArray.FromObject(games);
        }

      /*  public GameImport CreatedImport(List<(Guid gameInfoId, int quantity)> games)
        {
            foreach ((Guid, int) game in games)
            {
                var import = new GameImport
                {
                    Date = DateTime.Now,
                    GameInfoId =game.Item1,
                    Quantity = game.Item2
                };
                db.GameImports.Add(import);
            }
            db.SaveChanges();
            
        }*/

        public List<GameImport> CreatedImport(List<(Guid id, int Quantity)> importList)
        {
            List<GameImport> newGames = new List<GameImport>();
            foreach (var (id, quantity) in importList)
            {
                GameImport gameImport = new GameImport
                {
                    GameInfoId = id,
                    Quantity = quantity,
                    Date = DateTime.Now
                };
                newGames.Add(gameImport);
            }
            db.GameImports.AddRange(newGames);
            db.SaveChanges();
            return newGames;
        }
    }
}