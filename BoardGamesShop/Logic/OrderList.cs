﻿using BoardGamesShop.DAL;
using BoardGamesShop.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Logic
{
    public class OrderList
    {
        ShopContext db = new ShopContext();

      //  public JArray FullOrderList()
      //  {
      //      var orders = db.GameOrders.ToList().Select(x => x.ToJson());
      //      return JArray.FromObject(orders);
      //  }

        public JObject OrderById(Guid id)
        {
            var order = db.GameOrders.FirstOrDefault(x => x.Id == id);
            if (order == null)
                throw new Exception("Order not found.");
            return JObject.FromObject(order.ToJson());
        }

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        public JArray OrdersByUserId(Guid userId)
        {
            var orders = db.GameOrders.Where(x => x.UserId == userId)
                .ToList().Select(x => x.ToJson());
            if (orders==null)
                throw new Exception("Orders not found.");
            return JArray.FromObject(orders);
        }

        public JArray OrderByStatus(OrderStatus status)
        {
            var orders = db.GameOrders.Where(x => x.Status == status)
                .ToList().Select(x => x.ToJson());
            if (orders == null)
                throw new Exception("Orders not found.");
            return JArray.FromObject(orders);
        }

        public JArray OrderByDate(DateTime date)
        {
            var orders = db.GameOrders.Where(x => x.Date==date)
                .ToList().Select(x => x.ToJson());
            if (orders == null)
                throw new Exception("Orders not found.");
            return JArray.FromObject(orders);
        }

        public JArray OrdersSortedByDate()
        {
            var orders = db.GameOrders.OrderBy(x => x.Date).ToList().Select(x => x.ToJson());
            if (orders == null)
                throw new Exception("Orders not found.");
            return JArray.FromObject(orders);
        } 

        public GameOrder CreatedOrder(User user, string address, List<GameOrderInfo> newGameInfos)
        {
            var order = new GameOrder
            {
                Address = address,
                Date = DateTime.Now,
                Status = OrderStatus.Registered,
                User = user,
                GameOrderInfos = newGameInfos
            };
            return order;
        }

        public List<GameOrderInfo> gameOrderInfos(List<(GameInfo Game, int Count)> orderList)
        {
            List<GameOrderInfo> newGames = new List<GameOrderInfo>();
            foreach (var (game, count) in orderList)
            {
                var price = game.Price;
                GameOrderInfo gameOrderInfo = new GameOrderInfo
                {
                    Price = price, //необходимо, чтобы цена записывалась один раз и не менялась при изменении цены в инфе по играм
                    GameInfo = game,
                    Quantity = count
                };
                newGames.Add(gameOrderInfo);
            }
            db.GameOrderInfos.AddRange(newGames);
            db.SaveChanges();
            return newGames;
        }
    }
}