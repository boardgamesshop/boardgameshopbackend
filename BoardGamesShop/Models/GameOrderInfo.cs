﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Models
{
    public class GameOrderInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        public Guid GameOrderId { get; set; }
        public Guid GameInfoId { get; set; }
        [Required] public int Quantity { get; set; }
        [Required] public float Price { get; set; }

        public virtual GameOrder GameOrder { get; set; }
        public virtual GameInfo GameInfo { get; set; }
    }
}