﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace BoardGamesShop.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] [StringLength(100)] [Index(IsUnique = true)]  public string Login { get; set; }
        [Required] [StringLength(100)] public string Passhash { get; set; }
        [Required] public Position AccessLevel { get; set; }
        [Required] [StringLength(200)] public string FirstName { get; set; }
        [Required] [StringLength(200)] public string LastName { get; set; }
        [Required] [StringLength(200)] [Index(IsUnique = true)]  public string PhoneNumber { get; set; }
        [Required] [StringLength(200)] [Index(IsUnique = true)] public string Email { get; set; }

        public virtual ICollection<GameOrder> GameOrders { get; set; }

        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                Login,
                Passhash,
                AccessLevel,
                FirstName,
                LastName,
                PhoneNumber,
                Email,
            });
        }
    }
}