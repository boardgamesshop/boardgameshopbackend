﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace BoardGamesShop.Models
{
    public class GameImport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public Guid GameInfoId { get; set; }
        [Required] public int Quantity { get; set; }
        [Required] public DateTime Date { get; set; }

        public virtual GameInfo GameInfo { get; set; }

        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                GameInfoId,
                Quantity,
                Date,
                GameInfo.Name,
            });
        }
    }
}