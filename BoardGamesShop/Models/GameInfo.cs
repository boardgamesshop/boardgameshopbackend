﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Models
{
    public class GameInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] [StringLength(100)] [Index(IsUnique = true)] public string Name { get; set; }
        public string Picture { get; set; }
        [Required] public int MinNumberOfPlayers { get; set; }
        [Required] public int MaxNumberOfPlayers { get; set; }
        [Required] public int MinAge { get; set; }
        [Required] public int MinTime { get; set; }
        [Required] public int MaxTime { get; set; }
        [Required] [StringLength(400)] public string Description { get; set; }
        [Required] [StringLength(50)] public string Author { get; set; }
        [Required] public GameLevel Level { get; set; }
        [Required] public float Price { get; set; }

        public virtual GameInStock GameInStock { get; set; }
        public virtual ICollection<GameImport> GameImports { get; set; }
        
        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                Name,
                Picture,
                MinNumberOfPlayers,
                MaxNumberOfPlayers,
                MinAge,
                MinTime,
                MaxTime,
                Description,
                Author,
                Level,
                Price,
                GameInStock?.QuantityInStock,
            });
        }
    }
}