﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace BoardGamesShop.Models
{
    public class GameInStock
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        public Guid GameInfoId { get; set; }
        [Required] public int QuantityInStock { get; set; }

        public virtual GameInfo GameInfo { get; set; }

        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                GameInfoId,
                QuantityInStock,
            });
        }
    }
}