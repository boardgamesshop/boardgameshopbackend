﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Models
{
    public class UserComment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public Guid UserId { get; set; }
        [StringLength(2000)] public string Comment {get;set;}

        public virtual User User { get; set; }

        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                UserId,
                Comment,
                User.FirstName,
                User.LastName,
                User.Login,
            });
        }
    }
}