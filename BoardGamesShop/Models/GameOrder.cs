﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BoardGamesShop.Models
{
    public class GameOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public Guid UserId { get; set; }
        [Required] [StringLength(200)] public string Address { get; set; }
        [Required] public OrderStatus Status { get; set; }
        [Required] public DateTime Date { get; set; }
        public float Price => GameOrderInfos.Sum(x => x.Quantity * x.Price);

        public virtual User User { get; set; }
        public virtual ICollection<GameOrderInfo> GameOrderInfos { get; set; }

        public JObject ToJson()
        {
            return JObject.FromObject(new
            {
                Id,
                UserId,
                Address,
                Status,
                Date,
                Price,
                User.Login
            });
        }
    }
}