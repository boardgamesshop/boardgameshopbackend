namespace BoardGamesShop.Migrations
{
    using BoardGamesShop.Logic;
    using BoardGamesShop.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BoardGamesShop.DAL.ShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BoardGamesShop.DAL.ShopContext context)
        {
           // var user = new User { LastName = "dfgvhj", Login = "test1", Passhash = Encryption.GetPasshash("test1"), FirstName = "dfgh", AccessLevel = Position.Admin, PhoneNumber = "rtghj", Email = "fghjk" };
            var game = new GameInfo { Author = "hghfjgf", Description = "hgf", Level = GameLevel.Hard, MaxNumberOfPlayers = 8, MaxTime = 120, MinAge = 12, MinNumberOfPlayers = 2, MinTime = 30, Name = "hgfdfg", Picture = "gfhfg", Price = (float)234.65 };
            var game2 = new GameInfo { Author = "privet", Description = "privetuli", Level = GameLevel.Hard, MaxNumberOfPlayers = 8, MaxTime = 120, MinAge = 12, MinNumberOfPlayers = 2, MinTime = 30, Name = "hgfdfg", Picture = "gfhfg", Price = (float)234.65 };
            context.GameInfos.AddOrUpdate(game2);
           context.GameInStocks.AddOrUpdate(new GameInStock { GameInfo = game2, QuantityInStock = 0 });
            


            context.SaveChanges();
        }
    }
}
