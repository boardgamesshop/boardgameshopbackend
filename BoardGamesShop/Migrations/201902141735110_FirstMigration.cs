namespace BoardGamesShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GameImports",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        GameInfoId = c.Guid(nullable: false),
                        Quantity = c.String(nullable: false),
                        Date = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GameInfoes", t => t.GameInfoId, cascadeDelete: true)
                .Index(t => t.GameInfoId);
            
            CreateTable(
                "dbo.GameInfoes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        MinNumberOfPlayers = c.Int(nullable: false),
                        MaxNumberOfPlayers = c.Int(nullable: false),
                        MinAge = c.Int(nullable: false),
                        MinTime = c.Int(nullable: false),
                        MaxTime = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 400),
                        Author = c.String(nullable: false, maxLength: 50),
                        Level = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.GameInStocks",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        GameInfoId = c.Guid(nullable: false),
                        QuantityInStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GameInfoes", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.GameOrderInfoes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        GameOrderId = c.Guid(nullable: false),
                        GameInfoId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GameInfoes", t => t.GameInfoId, cascadeDelete: true)
                .ForeignKey("dbo.GameOrders", t => t.GameOrderId, cascadeDelete: true)
                .Index(t => t.GameOrderId)
                .Index(t => t.GameInfoId);
            
            CreateTable(
                "dbo.GameOrders",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Address = c.String(nullable: false, maxLength: 200),
                        Status = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100),
                        Passhash = c.String(nullable: false, maxLength: 100),
                        AccessLevel = c.Int(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 200),
                        LastName = c.String(nullable: false, maxLength: 200),
                        PhoneNumber = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Login, unique: true)
                .Index(t => t.PhoneNumber, unique: true)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GameOrders", "UserId", "dbo.Users");
            DropForeignKey("dbo.GameOrderInfoes", "GameOrderId", "dbo.GameOrders");
            DropForeignKey("dbo.GameOrderInfoes", "GameInfoId", "dbo.GameInfoes");
            DropForeignKey("dbo.GameInStocks", "Id", "dbo.GameInfoes");
            DropForeignKey("dbo.GameImports", "GameInfoId", "dbo.GameInfoes");
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Users", new[] { "PhoneNumber" });
            DropIndex("dbo.Users", new[] { "Login" });
            DropIndex("dbo.GameOrders", new[] { "UserId" });
            DropIndex("dbo.GameOrderInfoes", new[] { "GameInfoId" });
            DropIndex("dbo.GameOrderInfoes", new[] { "GameOrderId" });
            DropIndex("dbo.GameInStocks", new[] { "Id" });
            DropIndex("dbo.GameInfoes", new[] { "Name" });
            DropIndex("dbo.GameImports", new[] { "GameInfoId" });
            DropTable("dbo.Users");
            DropTable("dbo.GameOrders");
            DropTable("dbo.GameOrderInfoes");
            DropTable("dbo.GameInStocks");
            DropTable("dbo.GameInfoes");
            DropTable("dbo.GameImports");
        }
    }
}
