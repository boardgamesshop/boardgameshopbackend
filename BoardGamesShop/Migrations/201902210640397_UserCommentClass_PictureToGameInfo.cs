namespace BoardGamesShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCommentClass_PictureToGameInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserComments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Comment = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            AddColumn("dbo.GameInfoes", "Picture", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserComments", "UserId", "dbo.Users");
            DropIndex("dbo.UserComments", new[] { "UserId" });
            DropColumn("dbo.GameInfoes", "Picture");
            DropTable("dbo.UserComments");
        }
    }
}
