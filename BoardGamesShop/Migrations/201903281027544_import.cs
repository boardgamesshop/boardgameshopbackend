namespace BoardGamesShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class import : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GameImports", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.GameImports", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GameImports", "Date", c => c.String(nullable: false));
            AlterColumn("dbo.GameImports", "Quantity", c => c.String(nullable: false));
        }
    }
}
